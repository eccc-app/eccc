import Joi from "@hapi/joi";
import { CommonConfig } from "./CommonConfig.enum";
import { alphaNumericEnum } from "./util";
import { Deployment } from "../infrastructure";

export const CommonConfigSchema = {

  [CommonConfig.DEPLOYMENT]: alphaNumericEnum(Deployment)
    .required(),

};
