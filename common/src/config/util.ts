import Joi from "@hapi/joi";

export type ALPHA_NUMERIC = string | number;

export function enumAlphaKeys(obj: Record<ALPHA_NUMERIC, any>): Joi.StringSchema {
  return Joi.string().valid(...Object.keys(obj).filter(a => typeof a === "string"));
}

export function numericEnumValues(obj: Record<ALPHA_NUMERIC, ALPHA_NUMERIC>): Joi.NumberSchema {
  return Joi.number().valid(...Object.keys(obj).filter(a => typeof a === "number"));
}

export function alphaNumericEnum(obj: Record<ALPHA_NUMERIC, ALPHA_NUMERIC>): Joi.AlternativesSchema {
  return Joi.alternatives(enumAlphaKeys(obj), numericEnumValues(obj));
}
