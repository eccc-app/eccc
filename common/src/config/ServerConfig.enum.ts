export enum ServerConfig {

  /**
   * The primary HTTP port to expose the server on.  Numeric.
   */
  HTTP_PORT = "HTTP_PORT",

}
