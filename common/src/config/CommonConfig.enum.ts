export enum CommonConfig {
  
  /**
   * The current deployment.  See the `Deployment` enum for values.
   */
  DEPLOYMENT = "DEPLOYMENT",

}
