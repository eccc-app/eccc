import Joi from "@hapi/joi";
import { ServerConfig } from "./ServerConfig.enum";

export const ServerConfigSchema = {

  [ServerConfig.HTTP_PORT]: Joi.number().default(8080),

};
