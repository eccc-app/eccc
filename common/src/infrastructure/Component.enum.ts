/**
 * The different components in a deployment.
 */
export enum Component {
  
  /**
   * The entire system.
   */
  SYSTEM,

  /**
   * A site deployment.
   */
  SITE,

}
