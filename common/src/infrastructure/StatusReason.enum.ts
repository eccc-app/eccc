/**
 * Potential explanations why the system is in it's current state.
 */
export enum StatusReason {

  /**
   * Generic reason.
   */
  ONLINE,

  /**
   * System is deliberately disconnected - e.g. the on-site system between events.
   */
  OFFLINE,

  /**
   * System is down for maintenance.
   */
  MAINTENANCE,

  /**
   * System needs to be down for a deployment to take place.
   */
  DEPLOYMENT,

  /**
   * The system is actively being transported between locations.
   * May be fully offline, or be spotty.
   */
  TRANSPORTING,

}
