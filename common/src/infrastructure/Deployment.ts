/**
 * The instances of system deployments.
 */
export enum Deployment {
  CLOUD,
  ON_SITE,
}
