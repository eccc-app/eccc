/**
 * Describes the intended status of a component in the system.
 * Used to direct data in the system.
 */
export enum ComponentStatus {

  /**
   * The priority destination for data and control.
   */
  PRIMARY,

  /**
   * A secondary site that should be in frequent communication with a primary node.
   * May be just slightly behind the master.
   */
  LIVE_SECONDARY,

  /**
   * A node that is setup to have potentially failing replication.
   * May be quite out-of-sync with the primary node.
   */
  SLOW_REPLICA,

  /**
   * Explicitly refuses new data, unlike SLOW_REPLICA which may cache or redirect writes.
   * Used for maintenance or situations where a primary node will definitely be offline.
   */
  READ_ONLY,

  /**
   * Marks a component as intended to be completely offline, or desiring no requests.
   * Used for maintenance/upgrades, or when systems are being transported and will definitely be
   * disconnected.
   */
  OFFLINE,

}
