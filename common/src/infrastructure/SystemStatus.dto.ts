import { ComponentStatus } from "./ComponentStatus.enum";
import { StatusReason } from "./StatusReason.enum";

/**
 * Describes the status of a piece of the system,
 * with potential reasons behind the status.
 */
export class SystemStatus {

  /**
   * The intended status of the component.
   * Systems may be less responsive than scheduled.
   */
  public status: ComponentStatus;

  /**
   * Potentially describes why something is offline or in maintenance.
   */
  public reason: StatusReason;

  public constructor(status: ComponentStatus, reason: StatusReason) {
    this.status = status;
    this.reason = reason;
  }

}
