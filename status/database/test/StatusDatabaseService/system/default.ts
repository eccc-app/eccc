import test from "ava";
import { stub, mock, createStubInstance } from "sinon";
import { Repository } from "typeorm";
import { createModule } from "../helpers/createModule";
import { ScheduledStatus } from "@resultsvan/db";
import { StatusDatabaseService } from "../../../src/StatusDatabase.service";

test("defaults to online", async t => {
  const mockRepository = createStubInstance<Repository<ScheduledStatus>>(Repository, {
    find: stub(),
  });
  mockRepository.find.returns(Promise.resolve([]));
  const module = await createModule(mockRepository);
  const service = module.get<StatusDatabaseService>(StatusDatabaseService);
  const system = await service.system();
  t.is(system.reason, StatusDatabaseService.DEFAULT_SYSTEM_STATUS.reason);
  t.is(system.status, StatusDatabaseService.DEFAULT_SYSTEM_STATUS.status);
});
