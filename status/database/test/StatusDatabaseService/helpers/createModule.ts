import { SinonStubbedInstance } from "sinon";
import { Test, TestingModule } from "@nestjs/testing";
import { getRepositoryToken } from "@nestjs/typeorm";
import { Repository } from "typeorm";
import { ScheduledStatus } from "@resultsvan/db";
import { StatusDatabaseService } from "../../../src/StatusDatabase.service";

export async function createModule(
  mockRepository: SinonStubbedInstance<Repository<ScheduledStatus>>,
): Promise<TestingModule> {
  return Test.createTestingModule({
    providers: [
      {
        provide: getRepositoryToken(ScheduledStatus),
        useValue: mockRepository,
      },
      StatusDatabaseService,
    ],
  }).compile();
}
