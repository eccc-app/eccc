import { ClassProvider, Module } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";
import { ScheduledStatus } from "@resultsvan/db";
import { StatusService } from "@resultsvan/status";
import { StatusDatabaseService } from "./StatusDatabase.service";

const statusService: ClassProvider<StatusService> = {
  provide: StatusService,
  useClass: StatusDatabaseService,
}

@Module({
  imports: [
    TypeOrmModule.forFeature([
      ScheduledStatus,
    ]),
  ],
  providers: [
    statusService,
  ],
  exports: [
    statusService,
  ]
})
export class StatusDatabaseModule {
}
