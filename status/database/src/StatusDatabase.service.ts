import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { Repository, LessThan, MoreThan } from "typeorm";
import { SystemStatus, Component, ComponentStatus, StatusReason, Deployment } from "@resultsvan/common";
import { ScheduledStatus } from "@resultsvan/db";
import { StatusService } from "@resultsvan/status";

@Injectable()
export class StatusDatabaseService extends StatusService {

  public static readonly DEFAULT_SYSTEM_STATUS =
    new SystemStatus(ComponentStatus.PRIMARY, StatusReason.ONLINE);
  
  public static readonly DEFAULT_CLOUD_STATUS =
    new SystemStatus(ComponentStatus.PRIMARY, StatusReason.ONLINE);
  
  public static readonly DEFAULT_SITE_STATUS =
    new SystemStatus(ComponentStatus.OFFLINE, StatusReason.OFFLINE);

  private readonly statusRepository: Repository<ScheduledStatus>;

  public constructor(
    @InjectRepository(ScheduledStatus)
    statusRepository: Repository<ScheduledStatus>,
  ) {
    super();
    this.statusRepository = statusRepository;
  }

  public async system(): Promise<SystemStatus> {
    const statuses = await this.getCurrentStatus(Component.SYSTEM, null);
    if(statuses.length < 1) {
      return StatusDatabaseService.DEFAULT_SYSTEM_STATUS;
    }
    return new SystemStatus(statuses[0].status, statuses[0].reason);
  }

  public async cloud(): Promise<SystemStatus> {
    const statuses = await this.getCurrentStatus(Component.SITE, Deployment.CLOUD);
    if(statuses.length < 1) {
      return StatusDatabaseService.DEFAULT_CLOUD_STATUS;
    }
    return new SystemStatus(statuses[0].status, statuses[0].reason);
  }

  public async onSite(): Promise<SystemStatus> {
    const statuses = await this.getCurrentStatus(Component.SITE, Deployment.ON_SITE);
    if(statuses.length < 1) {
      return StatusDatabaseService.DEFAULT_CLOUD_STATUS;
    }
    return new SystemStatus(statuses[0].status, statuses[0].reason);
  }

  private getCurrentStatus(
    component: Component,
    deployment: Deployment | null,
  ): Promise<ScheduledStatus[]> {
    return this.statusRepository.find({
      select: [ "id", "status", "reason" ],
      where: {
        component: component,
        deployment: deployment,
        start: LessThan(Date.now()),
        end: MoreThan(Date.now()),
      },
      order: {
        start: "DESC",
      },
    });
  }

}
