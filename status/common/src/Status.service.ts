import { Injectable } from "@nestjs/common";
import { SystemStatus } from "@resultsvan/common";

/**
 * Tracks the current status of the system,
 * to allow data to be correctly directed to the system(s)
 * currently in charge of data.
 */
@Injectable()
export abstract class StatusService {
  
  /**
   * The overall status of the system.
   *
   * Used to take the entire system read-only/offline
   * for maintenance or deployments.
   */
  public async abstract system(): Promise<SystemStatus>;

  /**
   * The status of the cloud backend.
   * Used to control data going to on-site vs. the cloud system.
   */
  public async abstract cloud(): Promise<SystemStatus>;

  /**
   * The status of the on-site deployment.
   * Used to control data going to on-site vs. the cloud system.
   */
  public async abstract onSite(): Promise<SystemStatus>;

}
