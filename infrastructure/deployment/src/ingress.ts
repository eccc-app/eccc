import { Ingress } from "kubernetes-models/extensions/v1beta1";
import { Config } from "./Config";
import { WEB_APP_SERVER_NAME, WEB_APP_SERVER_PORT } from "./web-app/web-app.selector";
import { GRAPH_SERVER_NAME, GRAPH_SERVER_PORT } from "./graph-server/graph-server.selector";

export function ingress(
  config: Config,
): Ingress {
  const uiHost = config.production
    ? "ui.rst.vn"
    : `${config.sha}-ui.${config.rootDomain}`;
  const apiHost = config.production
    ? "api.rst.vn"
    : `${config.sha}-api.${config.rootDomain}`;
  return new Ingress({
    metadata: {
      name: config.env + "-" + "ingress",
      annotations: {
        "kubernetes.io/ingress.class": "nginx",
        "certmanager.k8s.io/cluster-issuer": "letsencrypt-prod",
      },
    },
    spec: {
      rules: [
        {
          host: uiHost,
          http: {
            paths: [
              {
                path: "/",
                backend: {
                  serviceName: config.env + "-" + WEB_APP_SERVER_NAME,
                  servicePort: WEB_APP_SERVER_PORT,
                },
              },
            ],
          },
        },
        {
          host: apiHost,
          http: {
            paths: [
              {
                path: "/",
                backend: {
                  serviceName: config.env + "-" + GRAPH_SERVER_NAME,
                  servicePort: GRAPH_SERVER_PORT,
                },
              },
            ],
          },
        },
      ],
    },
  });
}
