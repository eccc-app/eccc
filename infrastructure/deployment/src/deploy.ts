import { IncomingMessage } from "http";
import { cyan, green, red, blue, yellow, magenta } from "chalk";
import { Service } from "kubernetes-models/v1";
import { Ingress } from "kubernetes-models/extensions/v1beta1";
import { Deployment } from "kubernetes-models/apps/v1";
import * as k8s from "@kubernetes/client-node";
import { Config } from "./Config";
import { graphServerDeployment } from "./graph-server/graph-server.deployment";
import { graphServerService } from "./graph-server/graph-server.service";
import { webAppDeployment, webAppService } from "./web-app";
import { ingress } from "./ingress";

const MERGE_PATCH: [undefined, undefined, undefined, undefined, { headers: { [name: string]: string }}] = [
  undefined,
  undefined, 
  undefined,
  undefined,
  {
    headers: {
      "Content-type": k8s.PatchUtils.PATCH_FORMAT_JSON_MERGE_PATCH,
    },
  },
];

function ensureStr(val: any, explain: string): string {
  if(typeof val !== "string") {
    throw new Error(explain);
  }
  return val;
}

function env(key: string): string {
  return ensureStr(process.env[key], `Environment variable ${key} is required.`);
}

const ns = env("KUBE_NAMESPACE");

const config: Config = {
  rootDomain: "k8s.rst.vn",
  app: env("CI_PROJECT_PATH_SLUG"),
  env: env("CI_ENVIRONMENT_SLUG"),
  sha: env("CI_COMMIT_SHORT_SHA"),
  production: env("CI_COMMIT_REF_NAME") === "master",
};

const kc = new k8s.KubeConfig();
kc.loadFromDefault();
const core = kc.makeApiClient(k8s.CoreV1Api);
const apps = kc.makeApiClient(k8s.AppsV1Api);
const extensions = kc.makeApiClient(k8s.ExtensionsV1beta1Api);

const create = cyan("[CREATE]");
const update = cyan("[UPDATE]");
const remove = cyan("[DELETE]");
const start =         "START  ";
const success = green("SUCCESS");
const failure =   red("FAILURE");
const DEPLOYMENT = blue("deployment");
const SERVICE =  yellow("service   ");
const INGRESS = magenta("ingress   ");

async function act<T>(trans: string, ns: string, name: string | undefined, type: string, p: Promise<T>): Promise<T> {
  console.log(`${start} ${trans} ${type} ${name}`);
  try {
    const res = await p;
    console.log(`${success} ${trans} ${type} ${name}`);
    return res;
  } catch (err) {
    const res: IncomingMessage = err.response;
    const body = err.body;
    console.error(body);
    console.error(`${failure} ${trans} ${type} ${name}`);
    throw err;
  }
}

async function deploymentExists(ns: string, name: string): Promise<boolean> {
  try {
    await apps.readNamespacedDeployment(name, ns);
    return true;
  } catch (err) {
    return false;
  }
}

async function createNamespacedDeployment(ns: string, deployment: Deployment) {
  const name = ensureStr(deployment.metadata?.name, `Unnamed deployment provided`);
  if(await deploymentExists(ns, name)) {
    return act(update, ns, name, DEPLOYMENT, apps.patchNamespacedDeployment(name, ns, deployment as any, ...MERGE_PATCH));
  }
  return act(create, ns, name, DEPLOYMENT, apps.createNamespacedDeployment(ns, deployment as any));
}

function deleteNamespacedDeployment(ns: string, deployment: Deployment) {
  const name = ensureStr(deployment.metadata?.name, `Unnamed deployment provided.`);
  return act(remove, ns, name, DEPLOYMENT, apps.deleteNamespacedDeployment(name, ns));
}

async function serviceExists(ns: string, name: string): Promise<boolean> {
  try {
    await core.readNamespacedService(name, ns);
    return true;
  } catch (err) {
    return false;
  }
}

async function createNamespacedService(ns: string, service: Service) {
  const name = ensureStr(service.metadata?.name, `Unnamed service provided.`);
  if(await serviceExists(ns, name)) {
    return act(update, ns, name, SERVICE, core.patchNamespacedService(name, ns, service, ...MERGE_PATCH));
  }
  return act(create, ns, name, SERVICE, core.createNamespacedService(ns, service as any));
}

function deleteNamespacedService(ns: string, service: Service) {
  const name = ensureStr(service.metadata?.name, `Unnamed service provided.`);
  return act(remove, ns, name, SERVICE, core.deleteNamespacedService(name, ns));
}

async function ingressExists(ns: string, name: string): Promise<boolean> {
  try {
    await extensions.readNamespacedIngress(name, ns);
    return true;
  } catch (err) {
    return false;
  }
}

async function createNamespacedIngress(ns: string, ingress: Ingress) {
  const name = ensureStr(ingress.metadata?.name, `Unnamed ingress provided.`);
  if(await ingressExists(ns, name)) {
    return act(update, ns, name, INGRESS, extensions.patchNamespacedIngress(name, ns, ingress, ...MERGE_PATCH));
  }
  return act(create, ns, ingress.metadata?.name, INGRESS, extensions.createNamespacedIngress(ns, ingress as any));
}

function deleteNamespacedIngress(ns: string, ingress: Ingress) {
  const name = ensureStr(ingress.metadata?.name, `Unnamed ingress provided.`);
  return act(remove, ns, name, INGRESS, extensions.deleteNamespacedIngress(name, ns));
}

export async function up() {
  try {
    await Promise.all([
      createNamespacedDeployment(ns, graphServerDeployment(config)),
      createNamespacedService(ns, graphServerService(config)),
      createNamespacedDeployment(ns, webAppDeployment(config)),
      createNamespacedService(ns, webAppService(config)),
      createNamespacedIngress(ns, ingress(config)),
    ]);
  } catch (err) {
    process.exit(1);
  }
}

export async function down() {
  try {
    await Promise.all([
      deleteNamespacedDeployment(ns, graphServerDeployment(config)),
      deleteNamespacedService(ns, graphServerService(config)),
      deleteNamespacedDeployment(ns, webAppDeployment(config)),
      deleteNamespacedService(ns, webAppService(config)),
      deleteNamespacedIngress(ns, ingress(config)),
    ]);
  } catch (err) {
    console.error(err);
    process.exit(1);
  }
}
