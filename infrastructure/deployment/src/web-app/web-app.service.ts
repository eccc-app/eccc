import { Service } from "kubernetes-models/v1";
import { Config } from "../Config";
import { webAppSelector, WEB_APP_SERVER_NAME, WEB_APP_SERVER_PORT } from "./web-app.selector";

export function webAppService(
  config: Config,
): Service {
  return new Service({
    metadata: {
      name: config.env + "-" + WEB_APP_SERVER_NAME,
      labels: webAppSelector(config),
    },
    spec: {
      type: "ClusterIP",
      selector: webAppSelector(config),
      ports: [
        {
          protocol: "TCP",
          port: WEB_APP_SERVER_PORT,
          targetPort: WEB_APP_SERVER_PORT,
        },
      ],
    },
  });
}
