import { Deployment } from "kubernetes-models/apps/v1";
import { Config } from "../Config";
import { webAppContainer } from "./web-app.container";
import { webAppSelector, WEB_APP_SERVER_NAME } from "./web-app.selector";

export function webAppDeployment(
  config: Config,
): Deployment {
  return new Deployment({
    metadata: {
      name: config.env + "-" + WEB_APP_SERVER_NAME,
      labels: webAppSelector(config),
      annotations: {
        "app.gitlab.com/env": config.env,
        "app.gitlab.com/app": config.app,
      },
    },
    spec: {
      replicas: 1,
      selector: {
        matchLabels: webAppSelector(config),
      },
      strategy: {
        type: "RollingUpdate",
      },
      revisionHistoryLimit: 0,
      template: {
        metadata: {
          labels: webAppSelector(config),
        },
        spec: {
          containers: [
            webAppContainer(config),
          ],
        },
      },
    },
  });
}
