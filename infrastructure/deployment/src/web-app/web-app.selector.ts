import { Component, PACKAGE_NAME } from "../common";
import { Config } from "../Config";

export const WEB_APP_SERVER_NAME = "web-app";

export const WEB_APP_SERVER_PORT = 80;

export function webAppSelector(
  config: Config,
) {
  return {
    "app": config.env,
    "app.kubernetes.io/name": WEB_APP_SERVER_NAME,
    "app.kubernetes.io/version": config.sha,
    "app.kubernetes.io/component": Component.WEB_APP,
    "app.kubernetes.io/part-of": PACKAGE_NAME,
  };
};
