import { Container, IIoK8sApiCoreV1Probe } from "kubernetes-models/v1";
import { Config } from "../Config";
import { WEB_APP_SERVER_NAME, WEB_APP_SERVER_PORT } from "./web-app.selector";

export function webAppContainer(
  config: Config,
): Container {
  const probe: IIoK8sApiCoreV1Probe = {
    httpGet: {
      path: "/",
      port: WEB_APP_SERVER_PORT,
    },
    initialDelaySeconds: 2,
    periodSeconds: 2,
  };
  return new Container({
    name: config.env + "-" + WEB_APP_SERVER_NAME,
    image: `registry.gitlab.com/eccc-app/eccc/webapp:${config.sha}`,
    imagePullPolicy: "Always",
    ports: [
      { containerPort: WEB_APP_SERVER_PORT },
    ],
    livenessProbe: probe,
    readinessProbe: probe,
  });
}
