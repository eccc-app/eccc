/**
 * Global variables that are available to all services, e.g. commit SHA, etc.
 */
export interface Config {

  /**
   * A domain set with wildcards for deployment.
   */
  rootDomain: string;

  /**
   * The GitLab environment, e.g. "production"
   */
  env: string;

  /**
   * The GitLab app name, e.g. "eccc-app"
   */
  app: string;

  /**
   * The short commit SHA for the current build.
   */
  sha: string;

  /**
   * If the current deployment is the latest production run.
   */
  production: boolean;

}
