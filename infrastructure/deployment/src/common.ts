export enum Component {
  GQL_SERVER = "gql-server",
  WEB_APP = "web-app",
}

export const PACKAGE_NAME = "resultsvan-cloud";
