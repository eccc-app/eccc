import { Service } from "kubernetes-models/v1";
import { Config } from "../Config";
import { graphServerSelector, GRAPH_SERVER_NAME, GRAPH_SERVER_PORT } from "./graph-server.selector";

export function graphServerService(
  config: Config,
): Service {
  return new Service({
    metadata: {
      name: config.env + "-" + GRAPH_SERVER_NAME,
      labels: graphServerSelector(config),
    },
    spec: {
      type: "ClusterIP",
      selector: graphServerSelector(config),
      ports: [
        {
          name: "graphql",
          protocol: "TCP",
          port: 80,
          targetPort: GRAPH_SERVER_PORT,
        },
      ],
    },
  });
}
