import { Deployment } from "kubernetes-models/apps/v1";
import { Config } from "../Config";
import { graphServerContainer } from "./graph-server.container";
import { graphServerSelector, GRAPH_SERVER_NAME } from "./graph-server.selector";

export function graphServerDeployment(
  config: Config,
): Deployment {
  return new Deployment({
    metadata: {
      name: config.env + "-" + GRAPH_SERVER_NAME,
      labels: graphServerSelector(config),
      annotations: {
        "app.gitlab.com/env": config.env,
        "app.gitlab.com/app": config.app,
      },
    },
    spec: {
      replicas: 1,
      selector: {
        matchLabels: graphServerSelector(config),
      },
      strategy: {
        type: "RollingUpdate",
      },
      template: {
        metadata: {
          labels: graphServerSelector(config),
        },
        spec: {
          containers: [
            graphServerContainer(config),
          ],
        },
      },
    },
  });
}
