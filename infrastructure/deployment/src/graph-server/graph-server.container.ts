import { Container, IIoK8sApiCoreV1Probe } from "kubernetes-models/v1";
import { Config } from "../Config";
import { GRAPH_SERVER_NAME, GRAPH_SERVER_PORT } from "./graph-server.selector";

const portName = "graphql";

/**
 * Seconds.
 */
const HEALTH_CHECK_PERIOD = 5;

export function graphServerContainer(
  config: Config,
): Container {
  const probe: IIoK8sApiCoreV1Probe = {
    httpGet: {
      path: "/healthz",
      port: portName,
    },
    initialDelaySeconds: 20,
    periodSeconds: HEALTH_CHECK_PERIOD,
    //successThreshold: 2,
    failureThreshold: Math.floor(90 / HEALTH_CHECK_PERIOD),
  };
  return new Container({
    name: config.env + "-" + GRAPH_SERVER_NAME,
    image: `registry.gitlab.com/eccc-app/eccc/graph-server:${config.sha}`,
    imagePullPolicy: "Always",
    ports: [
      {
        name: portName,
        containerPort: GRAPH_SERVER_PORT,
      },
    ],
    livenessProbe: probe,
    readinessProbe: probe,
  });
}
