import { Component, PACKAGE_NAME } from "../common";
import { Config } from "../Config";

export const GRAPH_SERVER_NAME = "graph-server";

export const GRAPH_SERVER_PORT = 8550;

export function graphServerSelector(
  config: Config,
) {
  return {
    "app": config.env,
    "app.kubernetes.io/name": GRAPH_SERVER_NAME,
    "app.kubernetes.io/version": config.sha,
    "app.kubernetes.io/component": Component.GQL_SERVER,
    "app.kubernetes.io/part-of": PACKAGE_NAME,
  };
};
