import { join } from "path";
import { Module } from "@nestjs/common";
import { GraphQLModule } from "@nestjs/graphql";
import { StatusDatabaseModule } from "@resultsvan/status-database";
import { RootConfigModule } from "./config/root-config-module";
import { defaultGraphQLContext } from "./util/graphql-context";
import { GraphConfigModule } from "./config/graph-config.module";
import { TestingRootTypeORMModule } from "./db/root";
import { StatusModule } from "./status/status.module";
import { AppController } from "./app.controller";

@Module({
  imports: [
    RootConfigModule,
    TestingRootTypeORMModule,
    GraphConfigModule,
    GraphQLModule.forRoot({
      autoSchemaFile: join(process.cwd(), "src/schema.gql"),
      context: () => {
        const ctx = defaultGraphQLContext();
        return ctx;
      },
      fieldResolverEnhancers: [ "interceptors", "guards" ],
    }),
    StatusDatabaseModule,
    StatusModule,
  ],
  controllers: [
    AppController,
  ],
})
export class AppModule {
}
