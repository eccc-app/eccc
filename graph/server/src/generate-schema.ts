import { writeFile } from "fs";
import { join } from "path";
import { NestFactory } from "@nestjs/core";
import { GraphQLSchemaBuilderModule, GraphQLSchemaFactory } from "@nestjs/graphql";
import { printSchema } from "graphql";
import { StatusResolver } from "./status/status.resolver";

async function generateSchema() {
  const app = await NestFactory.create(GraphQLSchemaBuilderModule);
  await app.init();

  const gqlSchemaFactory = app.get(GraphQLSchemaFactory);
  const schema = await gqlSchemaFactory.create([
    StatusResolver,
  ]);
  const textSchema = printSchema(schema);
  writeFile(join(__dirname, "../schema.graphql"), textSchema, "utf8", () => {
    console.log(`Schema written.`);
  });
}

generateSchema();
