import { IncomingMessage } from "http";
import { ExecutionContext } from "@nestjs/common";
import { GqlExecutionContext } from "@nestjs/graphql";

export interface BaseGraphQLContext {

  req: IncomingMessage;

}

export class GraphQLContext {

  public constructor(
  ) {
  }

}

export function defaultGraphQLContext(): GraphQLContext {
  return new GraphQLContext();
}

export function getGraphQLContext(executionContext: ExecutionContext): BaseGraphQLContext & GraphQLContext {
  const gqlCtx = GqlExecutionContext.create(executionContext);
  const ctx = gqlCtx.getContext<BaseGraphQLContext & GraphQLContext>();
  return ctx;
}
