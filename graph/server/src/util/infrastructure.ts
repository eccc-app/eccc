import { registerEnumType } from "@nestjs/graphql";
import {
  ComponentStatus,
  Deployment,
  StatusReason,
} from "@resultsvan/common";

registerEnumType(Deployment, {
  name: "Deployment",
  description: "The instances of system deployments.",
});

registerEnumType(ComponentStatus, {
  name: "ComponentStatus",
  description: "Describes the current intended status of a system component.",
});

registerEnumType(StatusReason, {
  name: "StatusReason",
  description: "Potential explanations why the system is in it's current state",
});

export {
  ComponentStatus,
  Deployment,
  StatusReason,
};
