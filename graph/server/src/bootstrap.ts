import { NestFactory } from "@nestjs/core";
import { AppModule } from "./app.module";
import { GraphConfigService } from "./config/graph-config.service";

export async function bootstrap() {
  const app = await NestFactory.create(AppModule, {
    cors: {
      origin: [
        /\.resultsvan.com$/,
        /\.rst.vn$/,
      ],
    },
  });
  const configService: GraphConfigService = app.get(GraphConfigService);
  await app.listen(configService.httpPort);
  console.info(`Started server on port ${configService.httpPort}`);
}
