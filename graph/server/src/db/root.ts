import { TypeOrmModule } from "@nestjs/typeorm";
import { entities } from "@resultsvan/db";

export const TestingRootTypeORMModule = TypeOrmModule.forRoot({
  type: "sqlite",
  database: ":memory:",
  entities: entities,
  synchronize: true,
});
