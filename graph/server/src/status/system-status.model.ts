import { Field, ObjectType } from "@nestjs/graphql";
import { ComponentStatus, StatusReason } from "../util/infrastructure";

@ObjectType("SystemStatus", {
  description: "The status of a system component - availability, and potential reasons for the current state.",
})
export class SystemStatusModel {

  @Field(() => ComponentStatus, {
    description: "The intended availability of this component",
  })
  status: ComponentStatus;

  @Field(() => StatusReason, {
    description: "A potential reason for the current state, e.g. maintenance",
  })
  reason: StatusReason;

}
