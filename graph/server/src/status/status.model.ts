import { Field, ObjectType } from "@nestjs/graphql";
import { Deployment } from "../util/infrastructure";
import { SystemStatusModel } from "./system-status.model";

@ObjectType("Status", {
  description: "Contains the status of the system.",
})
export class StatusModel {

  @Field(() => Deployment, {
    description: "The server handling this request, used to determine cloud vs. on-site.",
  })
  deployment: Deployment;

  @Field(() => SystemStatusModel, {
    description: "The intended/scheduled status of the entire system, e.g. for maintenance windows.",
  })
  status: SystemStatusModel;

  @Field(() => SystemStatusModel, {
    description: "The intended/scheduled status of this deployment, e.g. marking cloud as read-only if deployed on-site.",
  })
  deploymentStatus: SystemStatusModel;

}
