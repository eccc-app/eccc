import { Module } from "@nestjs/common";
import { StatusDatabaseModule } from "@resultsvan/status-database";
import { GraphConfigModule } from "../config/graph-config.module";
import { StatusResolver } from "./status.resolver";

@Module({
  imports: [
    GraphConfigModule,
    StatusDatabaseModule,
  ],
  providers: [
    StatusResolver,
  ],
})
export class StatusModule {
}
