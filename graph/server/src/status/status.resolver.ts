import { Resolver, Query } from "@nestjs/graphql";
import { Deployment } from "@resultsvan/common";
import { StatusService } from "@resultsvan/status";
import { GraphConfigService } from "../config/graph-config.service";
import { StatusModel } from "./status.model";

@Resolver(StatusModel)
export class StatusResolver {

  private readonly config: GraphConfigService;
  private readonly status: StatusService;

  constructor(
    config: GraphConfigService,
    status: StatusService,
  ) {
    this.config = config;
    this.status = status;
  }

  @Query(() => StatusModel, {
    name: "status",
    description: "Get server information.  Use for routing to on-site vs. cloud deployments.",
  })
  public async getStatus(): Promise<StatusModel> {
    const deployment = this.config.deployment;
    const system = await this.status.system();
    const deploymentStatus = deployment === Deployment.CLOUD
      ? await this.status.cloud()
      : await this.status.onSite();
    return {
      deployment: deployment,
      status: system,
      deploymentStatus,
    };
  }

}
