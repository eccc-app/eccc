import { Controller, Get } from "@nestjs/common";

@Controller()
export class AppController {

  @Get("/healthz")
  public healthz(): string {
    return "Healthy!";
  }

}
