import Joi from "@hapi/joi";
import {
  CommonConfigSchema,
  ServerConfigSchema,
} from "@resultsvan/common";

export const configValidation = Joi.object({
  ...CommonConfigSchema,
  ...ServerConfigSchema,
});
