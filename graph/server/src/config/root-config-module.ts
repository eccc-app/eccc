import { join } from "path";
import { ConfigModule } from "@nestjs/config";
import { configValidation } from "./config.validation";

const DATA_DIR = "/data/";
const GRAPH_DIR = join(__dirname, "../../../");
const ROOT_DIR = join(GRAPH_DIR, "../../");

const CONFIG_COMMON = ".common.env";
const CONFIG_COMMON_DEFAULTS = ".common.defaults.env";

const CONFIG_SERVER = ".server.env";
const CONFIG_SERVER_DEFAULTS = ".server.defaults.env";

export const RootConfigModule = ConfigModule.forRoot({
  envFilePath: [
    join(DATA_DIR, CONFIG_SERVER),
    join(DATA_DIR, CONFIG_COMMON),
    join(GRAPH_DIR, CONFIG_SERVER),
    join(GRAPH_DIR, CONFIG_COMMON),
    join(GRAPH_DIR, CONFIG_SERVER_DEFAULTS),
    join(GRAPH_DIR, CONFIG_COMMON_DEFAULTS),
    join(ROOT_DIR, CONFIG_SERVER_DEFAULTS),
    join(ROOT_DIR, CONFIG_COMMON_DEFAULTS),
  ],
  validationSchema: configValidation,
});
