import {
  CommonConfig,
  Deployment,
  ServerConfig,
} from "@resultsvan/common";

export interface ConfigRecords {

  [CommonConfig.DEPLOYMENT]: Deployment;

  [ServerConfig.HTTP_PORT]: number;

}
