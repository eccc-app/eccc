import { Module } from "@nestjs/common";
import { ConfigModule } from "@nestjs/config";
import { GraphConfigService } from "./graph-config.service";

@Module({
  imports: [
    ConfigModule,
  ],
  providers: [
    GraphConfigService,
  ],
  exports: [
    GraphConfigService,
  ],
})
export class GraphConfigModule {
}
