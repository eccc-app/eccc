import { Injectable } from "@nestjs/common";
import { ConfigService } from "@nestjs/config";
import {
  CommonConfig,
  Deployment,
  ServerConfig,
} from "@resultsvan/common";
import { ConfigRecords } from "./config.interface";

@Injectable()
export class GraphConfigService {
  
  private readonly config: ConfigService<ConfigRecords>;

  constructor(
    config: ConfigService<ConfigRecords>,
  ) {
    this.config = config;
  }

  public get httpPort(): number {
    const port = this.config.get<number>(ServerConfig.HTTP_PORT);
    if(port === undefined) {
      throw new ReferenceError(`Config option ${ServerConfig.HTTP_PORT} required.`);
    }
    return port;
  }

  public get deployment(): Deployment {
    const deployment = this.config.get<keyof typeof Deployment | number>(CommonConfig.DEPLOYMENT);
    return this.getConfigEnum(CommonConfig.DEPLOYMENT, deployment, Deployment);
  }

  private getConfigEnum<E>(
    option: string,
    value: undefined | number | string, // keyof E
    obj: E,
  ) {
    if(value === undefined) {
      throw new ReferenceError(`Config option '${option}' missing.`);
    }
    const numeric = typeof value === "number" ? value : parseInt(value, 10);
    if(typeof value === "number" || !isNaN(numeric)) {
      return numeric;
    }
    if(Object.keys(obj).includes(value)) {
      return obj[value];
    }
    throw new ReferenceError(`Invalid config value for ${option}: '${value}' (${typeof value})`);
  }

}
