export default {
  typescript: {
    extensions: [
      "ts",
      "tsx",
    ],
    rewritePaths: {
      "src/": "out/src/",
      "test/": "out/test/",
    },
  },
};
