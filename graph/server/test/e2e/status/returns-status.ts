import anyTest, { TestInterface } from "ava";
import { Deployment } from "@resultsvan/common";
import { EndToEndContext, setupE2E } from "../helpers/setup";
import { gqlTest } from "../helpers/gql";

interface Context extends EndToEndContext {
}

const test = anyTest as TestInterface<Context>;
setupE2E(test);

test(
  "returns default status",
  gqlTest,
  `query {
    status {
      deployment
    }
  }`,
  {},
  {
    status: {
      deployment: Deployment[Deployment.CLOUD],
    },
  },
);
