import { TestInterface } from "ava";
import { Test, TestingModule } from "@nestjs/testing";
import { createTestClient, ApolloServerTestClient } from "apollo-server-testing";
import { INestApplication } from "@nestjs/common";
import { GraphQLModule } from "@nestjs/graphql";
import { AppModule } from "../../../src/app.module";

export interface EndToEndContext {
  server: TestingModule,
  app: INestApplication,
  gqlModule: GraphQLModule,
  apollo: ApolloServerTestClient,
}

export function setupE2E<
  C extends EndToEndContext,
>(
  test: TestInterface<C>,
) {
  test.beforeEach(async t => {
    t.context.server = await Test.createTestingModule({
      imports: [
        AppModule,
      ],
    }).compile();
    t.context.app = t.context.server.createNestApplication();
    await t.context.app.init();
    t.context.gqlModule = t.context.server.get<GraphQLModule>(GraphQLModule);
    t.context.apollo = createTestClient((t.context.gqlModule as any).apolloServer);
  });
}
