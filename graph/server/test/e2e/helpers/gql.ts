import { Macro } from "ava";
import { EndToEndContext } from "./setup";

export const gqlTest: Macro<[string, object, any], EndToEndContext> = async (
  t,
  query: string,
  variables: object,
  expected: any,
) => {
  const result = await t.context.apollo.query({
    query: query,
    variables: variables,
  });
  t.deepEqual(result.data, expected);
}
