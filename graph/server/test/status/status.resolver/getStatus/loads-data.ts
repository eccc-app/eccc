import anyTest, { TestInterface } from "ava";
import { Test } from "@nestjs/testing";
import { getRepositoryToken } from "@nestjs/typeorm";
import { ConfigService } from "@nestjs/config";
import { Repository } from "typeorm";
import { mock, SinonStub, SinonMock } from "sinon";
import {
  CommonConfig,
  ComponentStatus,
  Deployment,
  StatusReason, 
  SystemStatus,
} from "@resultsvan/common";
import { ScheduledStatus } from "@resultsvan/db";
import { StatusService } from "@resultsvan/status";
import { StatusDatabaseModule } from "@resultsvan/status-database";
import { RootConfigModule } from "../../../../src/config/root-config-module";
import { TestingRootTypeORMModule } from "../../../../src/db/root";
import { StatusModule } from "../../../../src/status/status.module";
import { StatusResolver } from "../../../../src/status/status.resolver";
import { GraphConfigModule } from "../../../../src/config/graph-config.module";

const DEFAULT_DEPLOYMENT = Deployment.CLOUD;
const STATUS_PRIMARY = new SystemStatus(ComponentStatus.PRIMARY, StatusReason.ONLINE);
const STATUS_SECONDARY_DEPLOY = new SystemStatus(ComponentStatus.LIVE_SECONDARY, StatusReason.DEPLOYMENT);
const STATUS_READ_ONLY_TRANSPORT = new SystemStatus(ComponentStatus.READ_ONLY, StatusReason.TRANSPORTING);

interface Context {
  statusService: SinonMock;
  configService: SinonMock;
  system: SinonStub;
  cloud: SinonStub;
  onSite: SinonStub;
  resolver: StatusResolver;
}

const test = anyTest as TestInterface<Context>;

test.beforeEach(async t => {
  const module = await Test.createTestingModule({
    imports: [
      RootConfigModule,
      TestingRootTypeORMModule,
      GraphConfigModule,
      StatusDatabaseModule,
      StatusModule,
    ],
  }).compile();
  t.context.configService = mock(module.get(ConfigService));
  const repository = mock(module.get<Repository<ScheduledStatus>>(getRepositoryToken(ScheduledStatus)));
  repository.expects("find").returns(null);
  const statusService = mock(module.get<StatusService>(StatusService));
  t.context.statusService = statusService;
  const system = statusService.expects("system").returns(STATUS_PRIMARY);
  const cloud = statusService.expects("cloud").returns(STATUS_SECONDARY_DEPLOY);
  const onSite = statusService.expects("onSite").returns(STATUS_READ_ONLY_TRANSPORT);
  const resolver = module.get<StatusResolver>(StatusResolver);
  t.context.system = system;
  t.context.cloud = cloud;
  t.context.onSite = onSite;
  t.context.resolver = resolver;
});

test(`returns deployment '${Deployment[DEFAULT_DEPLOYMENT]}' by default`, async t => {
  const status = await t.context.resolver.getStatus();
  t.is(status.deployment, DEFAULT_DEPLOYMENT);
});

test(`uses data from ${Deployment[DEFAULT_DEPLOYMENT]} by default`, async t => {
  const status = await t.context.resolver.getStatus();
  t.is(status.deploymentStatus, STATUS_SECONDARY_DEPLOY);
});

test(`uses data from ON_SITE if configured`, async t => {
  t.context.configService.expects("get").withArgs(CommonConfig.DEPLOYMENT).returns(Deployment.ON_SITE);
  const status = await t.context.resolver.getStatus();
  t.is(status.deployment, Deployment.ON_SITE);
})
