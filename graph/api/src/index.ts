import { createClient } from "../client/createClient";
export * from "../client/schema";

export const CLOUD_API_URL = "https://api.rst.vn:8550/graphql";
export const ON_SITE_API_URL = "https://local.api.rst.vn:8550/graphql";

export interface ECCCApiOptions {

  token?: string;

}

/**
 * Create a GraphQL API client for use with the ECCC/Results Van API.
 * 
 * @param url The API endpoint to connect to.
 * @param options Additional options to configure the API client.
 */
export function create(
  url: string,
  options: ECCCApiOptions = {},
) {
  const headers = new Headers();
  if(typeof options.token === "string") {
    headers.set("Authorization", `bearer ${options.token}`);
  }
  return createClient({
    fetcher: ({ query, variables }, fetch, qs) =>
      fetch(`${url}?${qs.stringify({ query, variables })}`, {
        headers,
      }).then(r => r.json()),
  });
}

export function cloud(options: ECCCApiOptions = {}) {
  return create(CLOUD_API_URL, options);
}

export function onSite(options: ECCCApiOptions = {}) {
  return create(ON_SITE_API_URL, options);
}

const _exports = {
  create,
  cloud,
  onSite,
};

export default _exports;
