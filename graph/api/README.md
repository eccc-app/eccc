# ECCC/Results Van API

This package contains a GraphQL API client for riders and applications for the Eastern Collegiate Cycling Conference.

Data has been extracted using [graphql-typed-client](https://github.com/helios1138/graphql-typed-client).

This is the only API wrapper officially supported by the ECCC.
The API will work with any other GraphQL client.

See [api.rst.vn/graphql][docs] for API documentation and a GraphQL explorer.

## Usage

(All examples are written using TypeScript)

```typescript
import api, { Deployment } from "@eccc/api";

// For use with the global API at api.rst.vn
const client = api.cloud();

// For use with the API hosted locally in the Results Van
// (local.api.rst.vn / 10.145.10.146)
const client2 = api.onSite();

// For use with a custom endpoint:
const client3 = api.create("https://dev.rst.vn:8550/graphql");

const result = await client.query({
  status: {
    deployment,
  },
});

// Handle the result:
console.log(`API responded from server ${result.status.deployment}`);
console.log(`Expected ${Deployment.CLOUD}`);
console.log(`Comparison: ${result.status.deployment === Deployment.CLOUD}`);
```

### Authentication

Most of the API queries use JWT authentication - entities, fields, and queries in the [GraphQL documentation][docs] are marked with the required authentication.

Tokens can be created by both the cloud API as well as the on-site servers.
For almost all use cases, tokens from one deployment can be used on the other.

However, newly created accounts may not have synced between the two, if we don't have a good network and are working mostly off-the-grid.

There are several scopes of tokens - most are for administrative privileges that most likely won't be encountered by riders.

However, riders will almost certainly encounter two authentication scopes: *common tokens* and *privileged tokens*.

Common tokens grant read access to most rider data, and have a very long lifespan and can be auto-renewed - ideal for mobile apps intended to be used on both cloud and on-site deployments.

Privileged tokens give read-access to sensitive data, and are needed for most write actions.  They last a short time (under 2 hours).

You should provide a JWT token when creating an API client:

```typescript
import api from "@eccc/api";

const client = api.cloud({
  token: "<jwt_token>",
});
```

### Enums

The API package exports enum definitions that use text-values, e.g. `Deployment.CLOUD = 'CLOUD'` - the values transmitted over the GraphQL API.

If you're building a React app, we'd recommend using the API's enum internally (e.g. in the store) for readability and ease of use.

However, the backend server uses numeric enums almost exclusively (`Deployment.CLOUD = 0`). These enums are all available in the `@resultsvan/common` package.

If you'd like to use numeric enums, you can define a mapping function:

```typescript
import api, { Deployment as ApiDeployment } from "@eccc/api";
import { Deployment } from "@resultsvan/common";

function toDeployment(deployment: ApiDeployment): Deployment {
  switch (deployment) {
    case ApiDeployment.CLOUD:
      return Deployment.CLOUD;
    case ApiDeployment.ON_SITE:
      return Deployment.ON_SITE;
  }
}

const client = api.cloud();
// ApiDeployment:
const _deployment = (await client.query({ status: { deployment: 1 }})).status.deployment;
// Deployment:
const deployment = toDeployment(_deployment);
```

[docs]: https://api.rst.vn/graphql
