/**
 * Fix for axios while we wait for ^0.2.1 to be published
 * merge: https://github.com/axios/axios/commit/b7e954eba3911874575ed241ec2ec38ff8af21bb
 * issue: https://github.com/axios/axios/issues/3219
 */
interface ProgressEvent {}
