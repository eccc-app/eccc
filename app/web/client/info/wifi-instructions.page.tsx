import React from "react";
import { Box, Heading, Paragraph } from "grommet";

const padTop = {
  top: "xsmall",
  bottom: "none",
};

export function WiFiInstructionsPage() {
  return (
    <React.Fragment>
      <Box id="wifi-instructions-page" align="center" pad={{ horizontal: "xsmall" }} fill="vertical" overflow="auto">
        <Box>
          <Heading level="3" textAlign="center" margin={{ vertical: "xsmall" }}>
            On-Site WiFi Instructions
          </Heading>
          <Paragraph size="small" margin={{ vertical: "none" }}>
            The Results Van brings our entire registration application on-site to all of our races.<br />
            This allows us to operate completely offline, in case we don't have an internet connection at a race.
          </Paragraph>
          <Paragraph size="small" margin={padTop}>
            When we do have an internet connection - which we normally do at bike parks -
            our cloud system (<a href="https://resultsvan.com/">resultsvan.com</a>) will forward
            requests to the on-site system.
          </Paragraph>
          <Paragraph size="small" margin={padTop}>
            Either way using our WiFi when at events provides the most reliable and fastest responses.
          </Paragraph>
          <Heading level="4" margin={padTop}>
            The Results Van App
          </Heading>
          <Paragraph size="small" margin={{ vertical: "none" }}>
            You can add this website as a Progressive Web Application, to your phone's homescreen.
            By installing the website on your device, it will save your profile offline and automatically connect to our
            servers whenever you are online or connected to our WiFi.
          </Paragraph>
          <Heading level="4" margin={padTop}>
            WiFi Network Details
          </Heading>
          <Paragraph margin={{ vertical: "none" }}>
            When on-site, find our network named "ResultsVan-Public".
          </Paragraph>
          <Paragraph margin={padTop}>
            Once connected, visit <a href="https://local.resultsvan.com/">https://local.resultsvan.com</a> in a web browser.
          </Paragraph>
          <Heading level="4" margin={padTop}>
            DNS Settings
          </Heading>
          <Paragraph size="small" margin={{ vertical: "none" }}>
            If you are unable to find our domain, you may need to adjust your DNS settings.
            You can set your computer or phone's DNS preferences to use our DNS server, <code>10.145.10.10</code>.
          </Paragraph>
          <Paragraph size="small" margin={padTop}>
            If that does not work, you can try visiting <a href="http://10.145.10.145/">http://10.145.10.145/</a>.<br />
            If directly connecting to <a href="http://10.145.10.145/">http://10.145.10.145/</a> shows our website,
            you do not have the correct DNS settings.
          </Paragraph>
          <Paragraph size="small" margin={padTop}>
            We strongly advise all users to try to fix DNS settings and use <a href="https://local.resultsvan.com/">local.resultsvan.com</a>,
            which ensures a secure connection via SSL.<br />
            If you are certain that you are connected to the official ResultsVan network, you may override security settings by using the following address:<br />
            <a href="http://10.145.10.145/?api1=10.145.10.146&ssl1=false">http://10.145.10.145/?api1=10.145.10.146&ssl1=false</a>
          </Paragraph>
        </Box>
      </Box>
    </React.Fragment>
  )
}

export default WiFiInstructionsPage;
