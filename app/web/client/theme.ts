import { grommet, ThemeType } from "grommet";
import { deepMerge } from "grommet/utils";

// https://coolors.co/433582-648bce-0dd3d3-202a25-e1f5e3

const customTheme: ThemeType = {
  global: {
    colors: {
      brand: {
        light: "#433582",
        dark: "#E1F5E3",
      },
      "accent-2": {
        light: "#648BCE",
        dark: "#648BCE",
      },
      "accent-3": {
        light: "#202A25",
        dark: "#202A25",
      },
      background: {
        light: "#FFFFFF",
        dark: "#202A25",
      },
      text: {
        light: "#202a25",
        dark: "#FFFFFF",
      },
      "text-strong": {
        light: "#121714",
        dark: "#FFFFFF",
      },
      "text-weak": {
        light: "#35463D",
        dark: "#CCCCCC",
      },
      "text-xweak": {
        light: "#8BA799",
        dark: "#999999",
      },
    },
  },
  button: {
    padding: {
      vertical: "5px",
      horizontal: "5px",
    },
    default: {
    },
    primary: {
      background: {
        light: "#E1F5E3",
        dark: "#202A25",
      },
      color: {
        light: "brand",
        dark: "brand",
      },
      padding: {
        vertical: "10px",
        horizontal: "20px",
      },
    }
  },
  paragraph: {
    small: {
      maxWidth: "432px",
    },
  },
};

export const theme = deepMerge(grommet, customTheme);
