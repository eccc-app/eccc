import React from "react";
import { useValues } from "kea";
import { Box, Menu, Text } from "grommet";
import { Connect, Cloud, Deliver } from "grommet-icons";
import { statusLogic } from "./status.logic";

function MenuLabel(props: {
  cloudOnline: null | boolean,
  onSiteOnline: null | boolean,
}) {
  if(props.cloudOnline) {
    return (
      <Box direction="row">
        <Cloud color="status-ok" />
        <Text color="white">Online</Text>
      </Box>
    );
  } else if(props.onSiteOnline) {
    return (
      <Box direction="row">
        <Deliver color="status-ok" />
        <Text color="white">Online</Text>
      </Box>
    );
  } else {
    return (
      <Box direction="row">
        <Connect color="status-critical" />
        <Text color="white">Disconnected</Text>
      </Box>
    )
  }
}

export function AppHeaderStatusButton(props: {
}) {
  const {
    cloudOnline,
    onSiteOnline,
  } = useValues(statusLogic);
  const label = (<MenuLabel
    cloudOnline={cloudOnline}
    onSiteOnline={onSiteOnline} />
  );
  return (
    <Menu
      label={label}
      items={[
        {
          label: "On-Site WiFi",
          href: "/wifi-instructions/",
        },
      ]} />
  );
}
