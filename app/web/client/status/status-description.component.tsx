import React from "react";
import { Box, Heading, Paragraph, Anchor } from "grommet";
import { ComponentStatus } from "@eccc/api";
import RoutedAnchor from "../components/routed-anchor.component";

function StatusFormat(params: {
  header: string,
  failure?: boolean,
  success?: boolean,
  children?: React.ReactNode,
}) {
  const headerColor = params.failure
    ? "status-critical"
    : params.success
      ? "status-ok"
      : "white";
  return (
    <Box background="accent-3">
      <Heading level="6" margin={{ vertical: "none" }} color={headerColor}>{ params.header }</Heading>
      <Paragraph margin={{ vertical: "none" }} size="small" color="white">
        { params.children }
      </Paragraph>
    </Box>
  );
}

export interface StatusDescriptionParams {

  /**
   * The overall status of the system.
   * Null if still loading.
   */
  systemStatus: null | ComponentStatus;

  /**
   * The status of the cloud API.
   */
  onlineStatus: null | ComponentStatus;

  /**
   * The status of the on-site API.
   */
  onSiteStatus: null | ComponentStatus;

  /**
   * If we are able to connect  to the cloud API.
   * Null if data hasn't been fetched yet.
   */
  cloudOnline: null | boolean;

  /**
   * If we are able to connect to the on-site API.
   * Null if data hasn't been fetched yet.
   */
  onsiteOnline: null | boolean;

}

export function StatusDescription(params: StatusDescriptionParams) {
  const anyApiLoaded = params.onlineStatus !== null || params.onSiteStatus !== null;
  if(params.onsiteOnline === false && params.cloudOnline === false) {
    return (
      <StatusFormat header="You are offline" failure>
        We are currently unable to connect to the Results Van.
        Your device may be offline, or our servers may be down.<br />
        Please check our <Anchor href="http://status.resultsvan.com/">status page</Anchor> for more information.
      </StatusFormat>
    )
  }
  if(params.systemStatus === null || !anyApiLoaded) {
    return (
      <StatusFormat header="Loading system status..." />
    );
  }
  if(params.systemStatus === ComponentStatus.OFFLINE) {
    return (
      <StatusFormat header="System is Offline">
        The system is currently offline, potentially for maintenance or upgrades.
      </StatusFormat>
    );
  }
  if(params.systemStatus === ComponentStatus.READ_ONLY) {
    return (
      <StatusFormat header="System is Read-Only">
        The system is read-only, potentially for maintenance or upgrades.
      </StatusFormat>
    );
  }
  if(params.onlineStatus === ComponentStatus.PRIMARY && params.cloudOnline) {
    return (
      <StatusFormat header="Connected to resultsvan.com" success>
        You're online and ready to go!
        Please check our <RoutedAnchor path="/wifi-instructions/">on-site WiFi instructions</RoutedAnchor>&nbsp;
        so you can connect to the van at races.
      </StatusFormat>
    );
  }
  if(params.onSiteStatus === ComponentStatus.PRIMARY && params.onsiteOnline) {
    return (
      <StatusFormat header="Connected to on-site WiFi" success>
        You're connected directly to the Results Van!
      </StatusFormat>
    );
  }
  if(
    params.onSiteStatus === ComponentStatus.PRIMARY
    && !params.onsiteOnline
    && params.onlineStatus !== ComponentStatus.LIVE_SECONDARY
  ) {
    return (
      <StatusFormat header="Online results may be outdated">
        The Results Van is currently live in the field.
        You may need to switch over to our on-site WiFi
        to register for races or check the results.
      </StatusFormat>
    );
  }
  return (
    <StatusFormat header="Unknown System Status">
    </StatusFormat>
  );
}
