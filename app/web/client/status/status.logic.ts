import { kea, MakeLogicType } from "kea";
import api, { QueryRequest, Deployment, ComponentStatus } from "@eccc/api";
import { configLogic } from "../config/config.logic";

interface StatusLogicValues {

  /**
   * If we have a live connection to the cloud-based API.
   */
  cloudOnline: null | boolean;

  /**
   * If we have a live connection to the on-site API.
   */
  onSiteOnline: null | boolean;

  /**
   * The intended status of the over-all system.
   */
  systemStatus: null | ComponentStatus;

  /**
   * The intended status of the cloud API.
   */
  cloudStatus: null | ComponentStatus;

  /**
   * The intended status of the on-site API.
   */
  onSiteStatus: null | ComponentStatus;

}

interface StatusLogicActions {

  fetchStatus: () => null;

  fetchCloudStatus: () => null;

  fetchCloudStatusSuccess: typeof fetchStatusSuccessCallback;

  fetchCloudStatusFailure: (err: Error) => { err: Error };

  fetchOnSiteStatus: () => null;

  fetchOnSiteStatusSuccess: typeof fetchStatusSuccessCallback;

  fetchOnSiteStatusFailure: (err: Error) => { err: Error };

}

interface StatusLogicProps {

}

type StatusLogicType = MakeLogicType<
  StatusLogicValues,
  StatusLogicActions,
  StatusLogicProps
>;

interface StatusCallbackData {

  /**
   * Which deployment this data is returned from/for.
   */
  deployment: Deployment;

  /**
   * The intended system-wide status.
   */
  systemStatus: ComponentStatus;

  /**
   * The intended status of this specific deployment.
   */
  deploymentStatus: ComponentStatus;

}

function fetchStatusSuccessCallback(
  deployment: Deployment,
  systemStatus: ComponentStatus,
  deploymentStatus: ComponentStatus,
): StatusCallbackData {
  return {
    deployment,
    systemStatus,
    deploymentStatus,
  };
}

const statusQuery: QueryRequest = {
  status: {
    deployment: 1,
    status: {
      status: 1,
    },
    deploymentStatus: {
      status: 1,
    },
  },
};

export const statusLogic = kea<StatusLogicType>({

  actions: {
    fetchStatus: true,
    fetchCloudStatus: true,
    fetchCloudStatusSuccess: fetchStatusSuccessCallback,
    fetchCloudStatusFailure: (err: Error) => { err },
    fetchOnSiteStatus: true,
    fetchOnSiteStatusSuccess: fetchStatusSuccessCallback,
    fetchOnSiteStatusFailure: (err: Error) => { err },
  },

  events: ({ actions, values }) => ({

    afterMount: [
      actions.fetchStatus,
    ],

  }),

  reducers: {

    cloudOnline: [null, {
      fetchCloudStatusSuccess: () => true,
      fetchCloudStatusFailure: () => false,
    }],

    onSiteOnline: [null, {
      fetchOnSiteStatusSuccess: () => true,
      fetchOnSiteStatusFailure: () => false,
    }],

    systemStatus: [null, {
      fetchOnSiteStatusSuccess: (_, { systemStatus }) => systemStatus,
      fetchCloudStatusSuccess: (_, { systemStatus }) => systemStatus,
    }],

    cloudStatus: [null, {
      fetchCloudStatusSuccess: (_, { deploymentStatus }) => deploymentStatus,
    }],

    onSiteStatus: [null, {
      fetchOnSiteStatusSuccess: (_, { deploymentStatus }) => deploymentStatus,
    }],

  },

  listeners: ({ actions }) => ({

    fetchStatus: async () => {
      if(configLogic.values.overrideURL) {
        // The override URL will be considered a cloud URL.
        return actions.fetchCloudStatus();
      }
      if(configLogic.values.forcedDeployment) {
        const deployment = configLogic.values.forcedDeployment;
        if(deployment === Deployment.CLOUD) {
          return actions.fetchCloudStatus();
        } else if(deployment === Deployment.ON_SITE) {
          return actions.fetchOnSiteStatus();
        }
      }
      actions.fetchCloudStatus();
      actions.fetchOnSiteStatus();
    },
    
    fetchCloudStatus: async () => {
      try {
        const client = api.create(configLogic.values.cloudURL);
        const res = await client.query(statusQuery);
        if(!res.data) {
          throw new Error("No data returned by cloud API.");
        }
        actions.fetchCloudStatusSuccess(
          res.data.status.deployment,
          res.data.status.status.status,
          res.data.status.deploymentStatus.status,
        );
      } catch (err) {
        console.error(err);
        actions.fetchCloudStatusFailure(err);
      }
    },

    fetchOnSiteStatus: async () => {
      try {
        const client = api.create(configLogic.values.onSiteURL);
        const res = await client.query(statusQuery);
        if(!res.data) {
          throw new Error("No data returned by cloud API.");
        }
        actions.fetchOnSiteStatusSuccess(
          res.data.status.deployment,
          res.data.status.status.status,
          res.data.status.deploymentStatus.status,
        );
      } catch (err) {
        console.error(err);
        actions.fetchOnSiteStatusFailure(err);
      }
    },

  }),

});
