import React from "react";
import { match, withRouter, RouteComponentProps } from "react-router-dom";
import { Anchor } from "grommet";

export interface RoutedAnchorProps extends RouteComponentProps {
  path: string;
  exact?: boolean;
  strict?: boolean;
  match: match;
  children: React.ReactNode;
}

function RoutedAnchor(props: RoutedAnchorProps) {
  return (
    <Anchor
      onClick={(e) => {
        e.preventDefault();
        props.history.push(props.path);
      }}
    >
      { props.children }
    </Anchor>
  );
}

export default withRouter<RoutedAnchorProps, typeof RoutedAnchor>(RoutedAnchor);
