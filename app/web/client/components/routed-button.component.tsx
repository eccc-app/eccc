import React from "react";
import { match, matchPath, withRouter, RouteComponentProps } from "react-router-dom";
import { Button } from "grommet";

export interface RoutedButtonProps extends RouteComponentProps {
  primary?: boolean;
  secondary?: boolean;
  path: string;
  exact?: boolean;
  strict?: boolean;
  color?: any;
  match: match;
  children: React.ReactNode;
}

function RoutedButton(props: RoutedButtonProps) {
  const pathMatch = matchPath(props.location.pathname, {
    exact: props.exact,
    path: props.path,
    strict: props.strict,
  });
  return (
    <Button
      active={!!pathMatch}
      primary={props.primary}
      secondary={props.secondary}
      onClick={(e) => {
        e.preventDefault();
        props.history.push(props.path);
      }}
      color={props.color}
    >
      { props.children }
    </Button>
  );
}

export default withRouter<RoutedButtonProps, typeof RoutedButton>(RoutedButton);
