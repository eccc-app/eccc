import React from "react";
import { Box, Heading, Paragraph, Button } from "grommet";

export interface InstallAppBannerProps {
  canInstall: boolean;
  installed: boolean;
}

export class InstallAppBanner extends React.Component<InstallAppBannerProps> {

  public render(): React.ReactNode {
    if(!this.props.canInstall) {
      return (<React.Fragment />);
    }
    if(this.props.installed) {
      return (<React.Fragment />);
    }
    return (
      <Box
        background="accent-2"
      >
        <Heading level="4" textAlign="center" color="white" margin={{ vertical: "none" }}>
          Install the Results Van Website
        </Heading>
        <Paragraph margin={{ vertical: "none" }} color="white" size="small">
          Our app automatically switches to the on-site server as soon as you connect to our WiFi.
        </Paragraph>
        <Box direction="row" justify="center">
          <Box>
            <Button
              primary
              size="medium"
              margin={{ vertical: "xsmall" }}
            >
              Add to Homescreen
            </Button>
          </Box>
        </Box>
      </Box>
    );
  }

}
