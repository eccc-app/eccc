import React from "react";
import { Box, Heading } from "grommet";
import { Car } from "grommet-icons";
import RoutedButton from "./routed-button.component";
import { AppHeaderStatusButton } from "../status/app-header-status-button.component";
import { HeaderAuthMenu } from "../auth/header-auth-menu.component";

const LOGO_ICON = Car;
const APP_NAME = "Results Van";

function AppNameHeader({
  logo,
  name,
}) {
  return (
    <RoutedButton path="/">
      <Box
        flex={false}
        direction="row"
        align="center"
        margin={{ left: "small" }}
        color="white"
      >
        { logo }
        <Heading level="4" margin={{ left: "small", vertical: "2px" }} color="white">
          { name }
        </Heading>
      </Box>
    </RoutedButton>
  );
}

function AppHeaderRight({
}) {
  return (
    <Box direction="row" align="center" margin={{ right: "small" }}>
      <AppHeaderStatusButton />
      <HeaderAuthMenu />
    </Box>
  );
}

function AppHeader({
}) {
  return (
    <Box
      tag="header"
      direction="row"
      background="brand"
      align="center"
      elevation="medium"
      justify="between"
      responsive={false}
      pad={{ vertical: "xsmall" }}
      style={{ position: "relative" }}
    >
      <AppNameHeader logo={<LOGO_ICON />} name={APP_NAME} />
      <AppHeaderRight />
    </Box>
  );
}

export default AppHeader;
