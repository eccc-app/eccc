import React from "react";
import { Footer, Anchor } from "grommet";
import { User, Schedule, Cart, SettingsOption } from "grommet-icons";

export default function AppFooter({
}) {
  return (
    <Footer background="brand">
      <Anchor
        href="/profile/"
        color="white"
        icon={<User />}
        label="My Profile" />
      <Anchor
        href="/races/"
        color="white"
        icon={<Schedule />}
        label="Races" />
      <Anchor
        href="/cart/"
        color="white"
        icon={<Cart />}
        label="Cart" />
      <Anchor
        href="/settings/"
        color="white"
        icon={<SettingsOption />}
        label="Settings" />
    </Footer>
  );
}
