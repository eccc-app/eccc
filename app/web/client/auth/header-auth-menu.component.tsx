import React from "react";
import { useActions, useValues } from "kea";
import { Box, Menu, Text } from "grommet";
import RoutedButton from "../components/routed-button.component";
import { authLogic } from "./auth.logic";

export function HeaderAuthMenu(props: {
}) {
  const {
    logout,
  } = useActions(authLogic);
  const {
    loggedIn,
  } = useValues(authLogic);

  if(!loggedIn) {
    return (
      <RoutedButton
        path="/login/"
        color="white"
      >
        Log In
      </RoutedButton>
    );
  }

  return (
    <Menu
      label="Account"
      items={[
        {
          label: "Logout",
          onClick: () => logout(),
        },
      ]} />
  );
}
