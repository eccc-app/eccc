import { MakeLogicType } from "kea";

interface AuthLogicValues {

  /**
   * `true` if we have a current user ID.
   * The user may need to refresh authentication.
   */
  loggedIn: boolean;

  /**
   * The display name for the current user.
   */
  name: null | string;

  /**
   * If we have a valid common JWT token.
   */
  commonAuth: boolean;

  /**
   * If we have a valid privileged token.
   */
  privilegedAuth: boolean;

}

interface AuthLogicActions {

  logout: () => null;

}

interface AuthLogicProps {

}

export type AuthLogicType = MakeLogicType<
  AuthLogicValues,
  AuthLogicActions,
  AuthLogicProps
>;
