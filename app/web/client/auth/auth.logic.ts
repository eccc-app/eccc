import { kea } from "kea";
import { AuthLogicType } from "./auth.logic.type";

export const authLogic = kea<AuthLogicType>({

  actions: {
    logout: true,
  },

  reducers: {

    loggedIn: [false, {
    }],

    name: [null, {
    }],

    commonAuth: [false, {
    }],

    privilegedAuth: [false, {
    }],

  },

});
