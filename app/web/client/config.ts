import { Config } from "./config/config.interface";
import { production } from "./config/prod.config";

export const config: Config = production;
