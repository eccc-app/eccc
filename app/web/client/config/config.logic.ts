import { kea, MakeLogicType } from "kea";
import { createBrowserHistory } from "history";
import qs from "qs";
import { Deployment } from "@eccc/api";
import { config } from "../config";

interface ConfigLogicValues {
  
  /**
   * An alternative API URL to use instead of the cloud/on-site URLs.
   * Null if an override URL shouldn't be used.
   */
  overrideURL: false | string;

  /**
   * If not-null, forces requests to use the set deployment,
   * e.g. `CLOUD` for cloud, `ON_SITE` for on-site.
   */
  forcedDeployment: null | Deployment;

  /**
   * The URL to use for the cloud API.
   */
  cloudURL: string;

  /**
   * The URL to use for the on-site API.
   */
  onSiteURL: string;

}

interface ConfigLogicActions {

}

interface ConfigLogicProps {

}

type ConfigLogicType = MakeLogicType<
  ConfigLogicValues,
  ConfigLogicActions,
  ConfigLogicProps
>;

const history = createBrowserHistory();
const location = history.location;
const search = qs.parse(location.search, {
  ignoreQueryPrefix: true,
});

function boolOrDefault<T>(param: any, defaultValue: T): boolean | T {
  return typeof param === "string" ? param === "true" : defaultValue;
}

function strOrDefault<T>(param: any, defaultValue: T): string | T {
  return typeof param === "string" ? param : defaultValue;
}

function intOrDefault<T>(param: any, defaultValue: T, base: number = 10): number | T {
  const parsed = parseInt(param, base);
  const parsedOrDefault = (typeof parsed === "number" && !isNaN(parsed)) ? parsed : defaultValue;
  return (typeof param === "number" && !isNaN(param)) ? param : parsedOrDefault;
}

function makeURL(
  https: boolean,
  host: string,
  port: number,
  path: string,
): string {
  return `${https ? "https" : "http"}://${host}:${port}/${path}`;
}

function deploymentOrDefault<T>(param: any, defaultValue: T): Deployment | T {
  if(typeof param === "string" && Object.keys(Deployment).includes(param)) {
    return Deployment[param];
  }
  return defaultValue;
}

export const configLogic = kea<ConfigLogicType>({

  actions: {
  },

  reducers: {

    overrideURL: [strOrDefault(search.api, false), {
    }],

    forcedDeployment: [deploymentOrDefault(search.deployment, null), {
    }],

    cloudURL: [makeURL(
      boolOrDefault(
        search.ssl,
        boolOrDefault(search.ssl0, config.cloudURL.ssl),
      ),
      strOrDefault(
        search.api,
        strOrDefault(search.api0, config.cloudURL.host),
      ),
      intOrDefault(
        search.port,
        intOrDefault(search.port0, config.cloudURL.port),
      ),
      config.cloudURL.path,
    ), {
    }],

    onSiteURL: [makeURL(
      boolOrDefault(search.ssl1, config.onSiteURL.ssl),
      strOrDefault(search.api1, config.onSiteURL.host),
      intOrDefault(search.port0, config.onSiteURL.port),
      config.onSiteURL.path,
    ), {
    }],
    
  },

});
