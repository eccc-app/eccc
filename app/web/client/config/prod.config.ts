import { Config } from "./config.interface";

export const production: Config = {
  cloudURL: {
    ssl: true,
    host: "api.rst.vn",
    port: 8550,
    path: "graphql",
  },
  onSiteURL: {
    ssl: true,
    host: "local.api.rst.vn",
    port: 8550,
    path: "graphql",
  },
};
