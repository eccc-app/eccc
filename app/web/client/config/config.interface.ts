export interface ConfigURL {

  /**
   * If the URL should be accessed over SSL.
   */
  ssl: boolean;

  /**
   * The hostname of the URL.
   */
  host: string;

  /**
   * The port to use.
   */
  port: number;

  /**
   * The path of the URL.
   */
  path: string;
}

export interface Config {
  
  /**
   * The URL to use for the GraphQL server deployed
   * to the cloud.
   */
  cloudURL: ConfigURL;

  /**
   * The URL to use for the GraphQL server deployed
   * on-site.
   */
  onSiteURL: ConfigURL;

}
