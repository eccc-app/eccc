import React from "react";
import { Heading, Text, Box, Button } from "grommet";

enum Location {
  HOME,
}

function locationName(location: Location): string {
  switch(location) {
    case Location.HOME:
      return "Home";
    default:
      throw new Error(`Unknown location ${location}`);
  }
}

function wifiOn(location: Location): boolean {
  switch(location) {
    case Location.HOME:
      return false;
    default:
      throw new Error(`Unknown location ${location}`);
  }
}

function LocationText(
  {location}: {
    location: Location,
  },
) {
  switch(location) {
    case Location.HOME:
      return (
        <div>
          <p>
            This means you can register for races and see results on&nbsp;
            <a href="http://resultsvan.com">resultsvan.com</a>.
            You probably shouldn't be using the on-site WiFi -
            please don't stalk the van and camp in my driveway.
          </p>
        </div>
      );
    default:
      throw new Error(`Unknown location ${location}`);
  }
}

function PWAInstallBox() {
  return (
    <Box
      background="accent-2"
    >
      <Text size="xlarge" color="white" textAlign="center">Have you added us to your home screen?</Text>
      <p>
        <Text color="white">
          You can add this website to your phone's home screen so it can be used offline.
        </Text>
      </p>
      <p>
        <Text color="white">
          This way you can show off your account to registration staff at races without
          an internet connection.
        </Text>
      </p>
      <p>
        <Text color="white">
          Additionally, the website will automatically switch between our online website
          and the local data on-site at races when connected to the Results Van WiFi,
          making sure you have the latest data.
        </Text>
      </p>
      <Box justify="center" direction="row">
        <Box>
          <Button primary size="large" margin={{ vertical: "small" }}>
            Add to Homescreen
          </Button>
        </Box>
      </Box>
    </Box>
  );
}

export function LandingPage({
}) {
  const pwaInstalled: boolean = false;
  const location: Location = Location.HOME;
  return (
    <React.Fragment>
      <Heading level="3" textAlign="center">
        A complete race registration and officiating platform
      </Heading>
      <p>
        <Text>
          We do things differently.
          Our system was designed for the oddities of collegiate cycling -
          registration stays open until the starting whistle blows,
          riders swap categories minutes before races start,
          and swap numbers mid-race.
        </Text>
      </p>
      <p>
        <Text>
          We have a 100% digital system for registration, officiating, and result publication
          that can handle all of these hurdles while being completely off the grid.
          It's designed for use in a muddle field in rural Vermont without internet or power.
        </Text>
      </p>
      <p>
        <Text>
          Ordinary is boring.  We try our hardest to be anything but normal.
        </Text>
      </p>
      <Box
        background="accent-2"
        direction="row"
        justify="between"
      >
        <Box>
          <p>
            <Text size="xlarge" color="white">
              Hosting a race?
            </Text>
          </p>
          <p>
            <Text size="large" color="white">
              Learn more about our services.
            </Text>
          </p>
        </Box>
        <Box justify="center" margin={{ right: "large" }}>
          <Button href="/services/" primary size="large">
            Learn More
          </Button>
        </Box>
      </Box>
      <div>
        <Text size="small" color="dark-4" margin={{ top: "small" }}>The van is currently...</Text>
      </div>
      <div>
        <Text size="large">{locationName(location)}</Text>
      </div>
      <LocationText location={location} />
      <Box
        justify="center"
        direction="row"
      >
        <Box>
          <Button href="/wifi-info/" primary disabled={!wifiOn(location)} size="large" margin={{ vertical: "small" }}>
            Connect to Results Van WiFi
          </Button>
        </Box>
      </Box>
      { pwaInstalled ? <div /> : <PWAInstallBox />}
    </React.Fragment>
  );
}

export default LandingPage;
