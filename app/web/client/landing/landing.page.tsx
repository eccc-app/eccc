import React from "react";
import { useValues } from "kea";
import { Box, Heading, Paragraph } from "grommet";
import { statusLogic } from "../status/status.logic";
import { StatusDescription } from "../status/status-description.component";
import { InstallAppBanner } from "../components/install-app-banner.component";
import RoutedButton from "../components/routed-button.component";
import RoutedAnchor from "../components/routed-anchor.component";

export function InstructionSection(props: {
  step: number,
  name: string
}) {
  return (
    <Heading level="4" margin={{ top: "xsmall", bottom: "none" }}>{ props.step }. { props.name }</Heading>
  );
}

export function LandingPage({

}) {
  const {
    systemStatus,
    cloudStatus,
    onSiteStatus,
    cloudOnline,
    onSiteOnline,
  } = useValues(statusLogic);
  return (
    <React.Fragment>
      <Box id="landing-page" align="center" pad={{ horizontal: "xsmall" }} fill="vertical" overflow="auto">
        <Box>
          <Heading level="3" textAlign="center" margin={{ vertical: "xsmall" }}>
            Results Van Registration and Officiating
          </Heading>
          <Paragraph textAlign="center" margin={{ vertical: "none" }}>
            "Results will be taped to the Results Van"
          </Paragraph>
          <InstructionSection step={1} name="connect to the van" />
          <StatusDescription
            systemStatus={systemStatus}
            onlineStatus={cloudStatus}
            onSiteStatus={onSiteStatus}
            cloudOnline={cloudOnline}
            onsiteOnline={onSiteOnline} />
          { /* <InstallAppBanner canInstall={true} installed={false} /> */ }
          <InstructionSection step={2} name="create an account" />
          <InstructionSection step={3} name="register a rider" />
          <Paragraph size="small" margin={{ vertical: "none" }}>
            Register your level, club/school affiliation, and license information.
          </Paragraph>
          <Paragraph size="small" margin={{ vertical: "none" }}>
            You can register multiple rider accounts under the same Results Van account -
            useful for parents registering multiple racers.
          </Paragraph>
          <InstructionSection step={4} name="register for races" />
          <Paragraph size="small" margin={{ vertical: "none" }}>
            Mark the races that you are planning to ride in, so your details will be available to our officials.
          </Paragraph>
          <RoutedButton primary path="/schedule/">Find Races</RoutedButton>
          <Heading level="5" margin={{ top: "xsmall", bottom: "none" }}>
            New ECCC Policy (Fall 2020)
          </Heading>
          <Paragraph size="small" margin={{ vertical: "none" }}>
            You <i>must</i> mark that you are racing on this website, so we can officiate efficiently.
            Don't worry, you can register on the way to the start-line, all the way until the start-whistle blows.
          </Paragraph>
          <InstructionSection step={5} name="pay" />
          <Paragraph size="small" margin={{ vertical: "none" }}>
            Select how you're paying for the races when you register.
            For the ECCC: you can pre-pay via BikeReg before the weekend, pay via cash/check at registration,
            or select that we should invoice your team (for pre-approved teams).
          </Paragraph>
          <InstructionSection step={6} name="submit your waver packet" />
          <Paragraph size="small" margin={{ vertical: "none" }}>
            We'll pre-fill waver packets - all you need to do is print, sign, and then bring them to registration.
            At the race you'll feed the packet through a document scanner so we can keep track of all paperwork.
          </Paragraph>
          <InstructionSection step={7} name="race" />
          <Paragraph size="small" margin={{ vertical: "none" }}>
            Our officials will check each rider entering the race.
            Expect to be warned if you have not paid or completed your registration.
          </Paragraph>
          <Paragraph size="small" margin={{ top: "xsmall", bottom: "none" }}>
            If you haven't completed your waver or haven't paid for multiple races in a row,
            expect to be denied entry or have your results hidden until you pay.
          </Paragraph>
          <InstructionSection step={8} name="check the results" />
          <Paragraph size="small" margin={{ vertical: "none" }}>
            Depending on the race and venue, we may have live race tracking and/or brackets.
            After races, we'll upload results directly to this website.
          </Paragraph>
          <Paragraph size="small" margin={{ top: "xsmall", bottom: "none" }}>
            Check the <RoutedAnchor path="/schedule/">list of races</RoutedAnchor>&nbsp;
            for live/final results from each race.
          </Paragraph>
        </Box>
      </Box>
    </React.Fragment>
  );
}

export default LandingPage;
