import * as React from "react";
import * as ReactDOM from "react-dom";
import { resetContext, getContext } from "kea";
import { Provider } from "react-redux";
import loadable from "@loadable/component";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import { Grommet, Main } from "grommet";
import { theme } from "./theme";

const AppHeader = loadable(() => import("./components/AppHeader"));
const LandingPage = loadable(() => import("./landing/landing.page"));
const WiFiInstructionsPage = loadable(() => import("./info/wifi-instructions.page"));
const AppFooter = loadable(() => import("./components/app-footer.component"));

resetContext({
  createStore: {
  },
  plugins: [
  ]
});

const App = () => (
  <Provider store={getContext().store}>
    <Router>
      <Grommet theme={theme} full>
        <Main>
          <AppHeader />
          <Switch>
            <Route path="/" exact><LandingPage /></Route>
            <Route path="/wifi-instructions/" exact><WiFiInstructionsPage /></Route>
          </Switch>
          <AppFooter />
        </Main>
      </Grommet>
    </Router>
  </Provider>
);

ReactDOM.render(<App />, document.getElementById("root"));
