const PnpWebpackPlugin = require("pnp-webpack-plugin");

module.exports = {

  entry: "./client/index.tsx",

  output: {
    filename: "[name].bundle.js",
    chunkFilename: "[name].bundle.js",
    path: __dirname + "/dist",
    publicPath: "/dist/",
  },

  devtool: "source-map",

  resolve: {
    plugins: [
      PnpWebpackPlugin,
    ],
    extensions: [ ".ts", ".tsx", ".js", ".jsx", ".json" ],
  },

  resolveLoader: {
    plugins: [
      PnpWebpackPlugin.moduleLoader(module),
    ],
  },

  module: {
    rules: [

      {
        test: /\.tsx?$/,
        loader: "awesome-typescript-loader",
        options: {
          configFileName: "tsconfig.browser.json",
        },
      },

      {
        enforce: "pre",
        test: /.js$/,
        loader: "source-map-loader",
      },


    ],
  },

  externals: {
    //"react": "React",
    //"react-dom": "ReactDOM",
    //"reflect-metadata": "Reflect",
  },

};
