const { entities } = require("./out/src/entities/entities");

module.exports = {
  type: "sqlite",
  database: "./schema.sqlite",
  entities,
  migrations: ["out/migrations/*.js"],
  cli: {
    migrationsDir: "migrations",
  },
  synchronize: false,
};
