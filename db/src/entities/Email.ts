import {
  Column, 
  CreateDateColumn,
  Entity,
  PrimaryGeneratedColumn,
} from "typeorm";

/**
 * All email addresses in the system.
 */
@Entity()
export class Email {

  /**
   * A unique identifier for the email.
   */
  @PrimaryGeneratedColumn("uuid")
  id: string;

  /**
   * The email address.
   */
  @Column("varchar", { length: 255 })
  address: string;

  /**
   * If the address has been confirmed via link.
   */
  @Column("boolean")
  verified: boolean;

  /**
   * When this email address was created in the system.
   */
  @CreateDateColumn()
  created: Date;

}
