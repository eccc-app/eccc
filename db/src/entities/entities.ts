import {
  Email,
  ScheduledStatus,
  User,
} from "./index";

export const entities = [
  Email,
  ScheduledStatus,
  User,
];
