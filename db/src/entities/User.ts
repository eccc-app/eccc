import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  OneToOne,
  PrimaryGeneratedColumn,
} from "typeorm";
import { Email } from "./Email";

/**
 * Each user account that can log in to the system.
 * Users can be administrators, and can also have multiple riders attached to a single user account.
 */
@Entity()
export class User {
  
  /**
   * A unique identifier for this user.
   */
  @PrimaryGeneratedColumn("uuid")
  public id: string;

  /**
   * The user's display name.
   */
  @Column("varchar", { length: 255 })
  public name: string;

  /**
   * The primary email address used by the user to login.
   */
  @OneToOne(type => Email)
  @JoinColumn()
  public primaryEmail: Email;

  /**
   * The user's hashed password.
   */
  @Column("varchar", { length: 255 })
  public password: string;

  /**
   * When the user created this account.
   */
  @CreateDateColumn()
  public registered: Date;

}
