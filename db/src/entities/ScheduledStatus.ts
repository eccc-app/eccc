import {
  Column,
  Entity, PrimaryGeneratedColumn,
} from "typeorm";
import { Component, ComponentStatus, Deployment, StatusReason } from "@resultsvan/common";

/**
 * Stores a pre-defined status to use.
 */
@Entity()
export class ScheduledStatus {

  /**
   * A unique identifier.
   */
  @PrimaryGeneratedColumn("uuid")
  id: string;

  /**
   * The start of the scheduled period.
   */
  @Column("date")
  start: Date;

  /**
   * The end of the scheduled period.
   */
  @Column("date")
  end: Date;

  /**
   * The component that this status applies to.
   */
  @Column("tinyint")
  component: Component;

  /**
   * The deployment that this status is specific to.
   * `NULL` if it's a system-wide status.
   */
  @Column("tinyint", {
    nullable: true,
  })
  deployment: Deployment;

  /**
   * The scheduled status of the component.
   */
  @Column("tinyint")
  status: ComponentStatus;

  /**
   * Any descriptive reason for the status,
   * e.g. "transporting" if the on-site deployment is being
   * transported to an event.
   */
  @Column("tinyint")
  reason: StatusReason;

}
