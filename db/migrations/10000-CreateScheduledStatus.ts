import {MigrationInterface, QueryRunner} from "typeorm";

export class CreateScheduledStatus1602603565242 implements MigrationInterface {
    name = 'CreateScheduledStatus1602603565242'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "scheduled_status" ("id" varchar PRIMARY KEY NOT NULL, "start" date NOT NULL, "end" date NOT NULL, "component" tinyint NOT NULL, "deployment" tinyint, "status" tinyint NOT NULL, "reason" tinyint NOT NULL)`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP TABLE "scheduled_status"`);
    }

}
